#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <ncurses.h>
#include <unistd.h>

#include "FreeImage.h"
#include "utils.h"
#include "vector.h"
#include "scene.h"
#include "ray.h"
#include "raytracer.h"
#include "globals.h"

// globals
light lights[3];
int num_lights = 1;
object scene[SCENE_SIZE];
int object_count;

color output_image[WIDTH*HEIGHT];
camera theCamera;

int img_index(int x, int y)
{
	return (x + (WIDTH*y));
}

void fill_red(color *img)
{
	for(int i=0; i<WIDTH; i++)
		for(int j=0; j<HEIGHT; j++) {
			img[img_index(i,j)].x = 1.0;
			img[img_index(i,j)].y = 0.0;
			img[img_index(i,j)].z = 0.0;
		}

}

void fill(color *img, color c)
{
	for(int i=0; i<WIDTH; i++)
		for(int j=0; j<HEIGHT; j++)
			img[img_index(i,j)] = c;

}

color color_gradient(color c0, color c1, int length, int alpha)
{
	float delta = (float)((float)1/(float)length);
	color grad_color;
	grad_color = vector_add(vector_float_mul(delta*alpha,c0), vector_float_mul(1-(delta*alpha),c1));
	return grad_color;
}

void horizontal_gradient(color *img, color c1, color c2)
{
	for(int k=0; k<HEIGHT; k++)
		for(int j=0; j<WIDTH; j++)
			img[img_index(j,k)] = color_gradient(c1, c2, HEIGHT, k);
}

void fill_rect(color *img, color c, point3 p1, point3 p2)
{
	for(int i=0; i<WIDTH; i++)
		for(int j=0; j<HEIGHT; j++)
			if(i > p1.x && i < p2.x && j > p1.y && j < p2.y) img[img_index(i,j)] = c;
}

void fill_circle(color *img, float radius, point3 center, color c)
{
	for(int i=0; i<WIDTH; i++)
		for(int j=0; j<HEIGHT; j++) {
			double dist = sqrt( pow(i-center.x,2) + pow(j-center.y,2) );
			if(dist < radius) img[img_index(i,j)] = c;
		}
}


void setup_scene_0()
{
	theCamera = init_camera(vector_init(2,1,3), vector_init(0,0.3,0), vector_init(0,1,0), 60, (float)WIDTH/(float)HEIGHT);

	lights[0].position = vector_init(-200, 170, 300);
	lights[0].col = vector_init(2, 2, 2);
	num_lights=1;

	object_count = 0;
	material mat;

	mat.kd = vector_init(.5, .5, .5);
	mat.ks = vector_init(.5,.5,.5);
	mat.shininess = 200;
	scene[object_count ++] = init_sphere(0,0,0,.3, mat);

	mat.kd = vector_init(.9,0,0);
	mat.ks = vector_init(.1,.1,.1);
	mat.shininess = 500;
	scene[object_count ++] = init_sphere(1,-.05,0,.15, mat);

	mat.kd = vector_init(0,1,0);
	mat.ks = vector_init(.0,.0,.0);
	mat.shininess = 0.01;
	scene[object_count ++] = init_sphere(0,1,0,.25, mat);

	mat.kd = vector_init(0,0,0.6);
	mat.ks = vector_init(.4,.4,.4);
	mat.shininess = 20;
	scene[object_count ++] = init_sphere(0,-.05,1,.20, mat);
}

void setup_scene_1()
{
	theCamera = init_camera(vector_init(2,1,3), vector_init(0,0.3,0), vector_init(0,1,0), 60, (float)WIDTH/(float)HEIGHT);

	lights[0].position = vector_init(-2, 1.7, 3);
	lights[0].col = vector_init(1, 1, 1);
	lights[1].position = vector_init(3, 2, 3);
	lights[1].col = vector_init(0.4, 0.4, 0.4);
	lights[2].position = vector_init(4,3,-10);
	lights[2].col = vector_init(0.5, 0.5, 0.5);

	num_lights=3;

	object_count = 0;
	material mat;

	mat.kd = vector_init(.5,.5,.5);
	mat.ks = vector_init(.5,.5,.5);
	mat.shininess = 20;
	mat.reflect_coef = 0.8;
	scene[object_count ++] = init_sphere(0,0,0,.3, mat);

	mat.kd = vector_init(0,0,1);
	mat.ks = vector_init(.5,.5,.5);
	mat.shininess = 20;
	mat.reflect_coef = 0.5;
	scene[object_count ++] = init_sphere(0,-.05,1,.20, mat);

	mat.ks = vector_init(.1,.1,.1);
	mat.kd = vector_init(0,1,0);
	mat.reflect_coef = 0.04;
	scene[object_count ++] = init_sphere(0,1,0,.15, mat);

	mat.kd = vector_init(1,0,0);
	mat.shininess = 500;
	scene[object_count ++] = init_sphere(1,-.05,0,.15, mat);

	mat.kd = vector_init(.5,1,.7);
	scene[object_count ++] = init_plane(0,1,0,0.2, mat);
}

void setup_scene_2()
{
	theCamera = init_camera(vector_init(2,1,3), vector_init(0,0.3,0), vector_init(0,1,0), 60, (float)WIDTH/(float)HEIGHT);

	lights[0].position = vector_init(-2, 1.7, 3);
	lights[0].col = vector_init(1, 1, 1);
	lights[1].position = vector_init(3, 2, 3);
	lights[1].col = vector_init(0.4, 0.4, 0.4);
	lights[2].position = vector_init(4,3,-10);
	lights[2].col = vector_init(0.5, 0.5, 0.5);

	num_lights=3;

	object_count = 0;
	material mat;
	mat.shininess = 20;
	mat.kd = vector_init(1,1,1);
	mat.ks = vector_init(.5,.5,.5);
	mat.reflect_coef = 0.8;
	scene[object_count ++] = init_sphere(0,0,0,.3, mat);

	mat.reflect_coef = 0.5;
	mat.shininess = 20;
	mat.kd = vector_init(0,0,1);
	scene[object_count ++] = init_sphere(0,-.05,1,.20, mat);

	mat.reflect_coef = 0.04;
	mat.ks = vector_init(.1,.1,.1);
	mat.kd = vector_init(0,1,0);
	scene[object_count ++] = init_sphere(0,1,0,.15, mat);

	mat.kd = vector_init(1,0,0);
	mat.shininess = 500;
	scene[object_count ++] = init_sphere(1,-.05,0,.15, mat);

	mat.kd = vector_init(.5,1,.7);
	scene[object_count ++] = init_plane(0,1,0,0.2, mat);


	mat.reflect_coef = 0.0;
	mat.ks = vector_init(.1,.1,.1);
	mat.kd = vector_init(1,1,0);
	scene[object_count ++] = init_cylinder(vector_init(.0,.0,.0), vector_init(1,0,0), 1, .05, mat);
	scene[object_count ++] = init_cylinder(vector_init(.0,.0,.0), vector_init(0,1,0), 1, .05, mat);
	scene[object_count ++] = init_cylinder(vector_init(.0,.0,.0), vector_init(0,0,1), 1, .05, mat);

}

void setup_scene_3()
{
	theCamera = init_camera(vector_init(2,1,3), vector_init(0,0.3,0), vector_init(0,1,0), 60, (float)WIDTH/(float)HEIGHT);

	lights[0].position = vector_init(-2, 1.7, 3);
	lights[0].col = vector_init(1, 1, 1);
	lights[1].position = vector_init(3, 2, 3);
	lights[1].col = vector_init(0.4, 0.4, 0.4);
	lights[2].position = vector_init(4,3,-10);
	lights[2].col = vector_init(0.5, 0.5, 0.5);

	num_lights=3;

	object_count = 0;
	material mat;
	mat.shininess = 20;
	mat.kd = vector_init(1,0,0);
	mat.ks = vector_init(.5,.5,.5);
	mat.reflect_coef = 0.8;
	for(int i=0; i<SCENE_SIZE; i++) {
		mat.kd = vector_init(((float)(rand()%255))/255.f,((float)(rand()%255))/255.f,((float)(rand()%255))/255.f);
		scene[object_count ++] = init_sphere(((float)(rand()%8))/4.f,((float)(rand()%8))/4.f,((float)(rand()%8))/4.f,.1, mat);
	}
}

void setup_scene_4()
{
	theCamera = init_camera(vector_init(2,1,3), vector_init(0,0.3,0), vector_init(0,1,0), 60, (float)WIDTH/(float)HEIGHT);
	lights[0].position = vector_init(0, 1.7, 1);
	lights[0].col = vector_init(1, 1, 1);
	lights[1].position = vector_init(3, 2, 3);
	lights[1].col = vector_init(0.4, 0.4, 0.4);
	lights[2].position = vector_init(4,3,-1);
	lights[2].col = vector_init(0.5, 0.5, 0.5);

	num_lights=3;

	object_count = 0;
	material mat;
	mat.kd = vector_init(.5, .5, .5);
	mat.ks = vector_init(.5, .5, .5);
	mat.shininess = 200;
	scene[object_count ++] = init_sphere(0,0,0,.3, mat);

	mat.kd = vector_init(.9,0,0);
	mat.ks = vector_init(.1,.1,.1);
	mat.shininess = 500;
	scene[object_count ++] = init_sphere(1,-.05,0,.15, mat);

	mat.kd = vector_init(0,1,0);
	mat.ks = vector_init(.0,.0,.0);
	mat.shininess = 0.01;
	scene[object_count ++] = init_sphere(0,1,0,.25, mat);

	mat.kd = vector_init(0,0,0.6);
	mat.ks = vector_init(.4,.4,.4);
	mat.shininess = 20;
	scene[object_count ++] = init_sphere(0,-.05,1,.20, mat);

	/* bleu */
	mat.kd = vector_init(.5,0.9,.7);
	mat.ks = vector_init(.01,.01,.01);
	mat.shininess = 50;
	scene[object_count ++] = init_plane(0,1,0,0.2, mat);

	/* rouge */
	mat.kd = vector_init(.8,0.09,.07);
	mat.ks = vector_init(.2,.2,.1);
	mat.shininess = 10;
	scene[object_count ++] = init_plane(1,0.0, -1.0, 2, mat);

	/* vert */
	mat.kd = vector_init(0.1,0.3,.05);
	mat.ks = vector_init(.5,.5,.5);
	mat.shininess = 100;
	scene[object_count ++] = init_plane(0.3,-0.2, 1, 3, mat);

}


void setup_scene(int id)
{
	if(id == 0) {
		setup_scene_0();
		return;
	}
	if(id == 1) {
		setup_scene_1();
		return;
	}
	if(id == 2) {
		setup_scene_2();
		return;
	}
	if(id == 3) {
		setup_scene_3();
		return;
	}
	if(id == 4) {
		setup_scene_4();
		return;
	}
}

void export_freeimage(int argc, char ** argv)
{
	char basename[256]="render";
	if(argc >3) strcpy(basename, argv[2]);
	FreeImage_Initialise();
	FIBITMAP *bitmap = FreeImage_Allocate(WIDTH, HEIGHT, 32, FI_RGBA_RED_MASK,
	                                      FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
	if (bitmap) {
		// bitmap successfully created!

		// Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		color *ptr = output_image;
		int bytespp = FreeImage_GetLine(bitmap) / FreeImage_GetWidth(bitmap);
		for(unsigned y = 0; y < FreeImage_GetHeight(bitmap); y++) {
			BYTE *bits = FreeImage_GetScanLine(bitmap, y);
			for(unsigned x = 0; x < FreeImage_GetWidth(bitmap); x++) {
				// Set pixel color to green with a transparency of 128
				*ptr = vector_clamp(*ptr, 0.f, 1.f);
				bits[FI_RGBA_RED] = ptr->x*255;
				bits[FI_RGBA_GREEN] = ptr->y*255;
				bits[FI_RGBA_BLUE] = ptr->z*255;
				bits[FI_RGBA_ALPHA] = 255;
				// jump to next pixel
				bits += bytespp;
				ptr++;
			}
		}

		// FreeImage_Save(FREE_IMAGE_FORMAT fif, FIBITMAP *dib, const char *filename, int flags FI_DEFAULT(0));
		char filename[256];
		strcpy(filename, basename);
		strcat(filename, ".png");
		if (FreeImage_Save(FIF_PNG, bitmap, filename, 0))
			printf("Task saved as %s\n", filename);
		FreeImage_Unload(bitmap);
	}

	FreeImage_DeInitialise();

}

int check_job_done(stats_job *stats[], int n)
{
	int n_end=0;
	for(int i=0; i<n; i++) {
		n_end += stats[i]->curr_rows == stats[i]->tot_rows;
	}
	return (n_end == n);
}

int main(int argc, char ** argv)
{
	int scene_id = 1;
	int num_threads = 1;
	bool use_octree = false;
	if(argc == 1) {
		printf("Raytracer usage: %s scene_id num_threads\n",argv[0]);
		return 1;
	}
	if(argc >1) scene_id = atoi(argv[1]);
	if(argc >2) num_threads = atoi(argv[2]);

	// scene_id 0, 1, 2, 3 is for tp 1 tests;
	switch(scene_id) {
	case 0:
		fill_red(output_image);
		break;
	case 1:
		fill(output_image, vector_init(0.7,0.3,0.1));
		fill_rect(output_image, vector_init(0.2, 0.6, 0.7), vector_init(WIDTH/4, HEIGHT/5,0), vector_init(WIDTH/3, HEIGHT/4,0));
		break;
	case 2:
		horizontal_gradient(output_image, vector_init(0,0,1), vector_init(1,1,1));
		break;
	case 3:
		horizontal_gradient(output_image, vector_init(0,0,1), vector_init(1,1,1));
		fill_circle(output_image, WIDTH/20, vector_init(4*WIDTH/5, 4*HEIGHT/5, 0), vector_init(1, 1, 0));
		fill_rect(output_image, vector_init(0.1, 0.8, 0.4), vector_init(0, 0, 0), vector_init(WIDTH, HEIGHT/4,0));
		break;
	case 4:
		break;
	default:
		setup_scene(scene_id-5);

		/* ncurses init */
		initscr();
		noecho();

		/* threading init */
		int unit_i = WIDTH / num_threads;
		pthread_t threads[num_threads];
		stats_job *thread_stats[num_threads];
		int cpt_thr=0;

		/* launching threads */
		for(int i=1; i<=num_threads; i++) {

			/* creating a context to forward task data */
			render_job *context = (render_job*)malloc(sizeof(render_job));
			stats_job *stat = (stats_job*)malloc(sizeof(stats_job));
			context->start_x = (i-1)*unit_i;
			context->end_x = (i)*unit_i;
			context->cam = theCamera;
			context->scene = scene;
			context->n_obj = object_count;
			context->li = lights;
			context->n_li = num_lights;
			context->img = output_image;
			context->stats = stat;
			stat->tot_rows++;

			/* firing the rocket */
			thread_stats[cpt_thr] = stat;
			pthread_create(&(threads[cpt_thr]), NULL, &raytrace, context);
			cpt_thr++;
		}

		/* show threads status, and wait for them to terminate */
		int end=0;
		while(!end) {
			clear();
			for(int k=0; k<num_threads; k++) {
				stats_job *stat = thread_stats[k];
				if(stat->curr_rows == stat->tot_rows) {
					mvprintw(k+1, 1, "THR. %d, DONE, AVG. %f s., ETA. %f s., TOT. %f s.\n",k+1,stat->curr_rows,stat->tot_rows,stat->mid_time,stat->eta_time,stat->sum_time);
					end++;
				} else {
					mvprintw(k+1, 1, "THR. %d, %d / %d, AVG. %f s., ETA. %f s., TOT. %f s.\n",k+1,stat->curr_rows,stat->tot_rows,stat->mid_time,stat->eta_time,stat->sum_time);
				}

			}
			refresh();
			/* check if every thread has completed its task */
			end = check_job_done(thread_stats, num_threads);
			sleep(1);
		}

		/* ncurses stop */
		endwin();

		break;
	}

	export_freeimage(argc,argv);
	// pas de bras, pas de chocolat	
	return 0;
}
