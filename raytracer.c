#include "raytracer.h"
#include "globals.h"
#include "utils.h"

#include <time.h>
#include <ncurses.h>
#include <math.h>

inline int intersect_plane(ray *r, object plane, intersection *intr)
{
	float num = (vector_dot(r->orig, plane.normal)+plane.dist);
	float den = vector_dot(r->dir, plane.normal);
	float t = -(num / den);

	if(t > r->tmin && t < r->tmax) {
		intr->position = ray_at(*r, t);
		intr->normal = plane.normal;
		intr->mat = plane.mat;
		r->tmax = t;
		return 1;
	}
	return 0;
}

inline int intersect_sphere(ray *r, object sphere, intersection *intr)
{
	vec3 a_diff = vector_minus(r->orig, sphere.center);
	float a = vector_dot(r->dir, r->dir);
	float b = 2.0f*(vector_dot(r->dir, a_diff));
	float c = (vector_dot(a_diff,a_diff) - (sphere.radius*sphere.radius));

	float delta = (b*b)-(4.0f*a*c);

	if(delta >= 0) {
		float sqdelta = sqrtf(delta);
		float t1 = ((-b) - sqdelta)/(2.0f*a);
		float t2 = ((-b) + sqdelta)/(2.0f*a);
		float t = min(t1, t2);

		if(t > r->tmin && t < r->tmax) {
			intr->position = ray_at(*r, t);
			intr->normal = vector_normalized(vector_minus(intr->position, sphere.center));
			intr->mat = sphere.mat;
			r->tmax = t;
			return 1;
		}
	}
	return 0;
}

int is_viewable(object *scene, int n_obj, point3 orig, point3 dest)
{
	intersection intr;
	vec3 fdir = vector_minus(dest, orig);
	vec3 dir = vector_normalized(fdir);
	ray r;
	/* suppression du phénomène d'acné : on empêche le rayon d'intersecter avec l'objet d'origine */
	point3 start = vector_add(vector_float_mul(0.0001f, dir), orig);
	ray_init(&r, start, dir, 0, vector_norm(fdir), 0.0f);

	int int_count=0;
	for(int k=0; (k<n_obj) && (!int_count); k++) {
		object obj = scene[k];
		switch(obj.type) {
		case PLANE:
			int_count += intersect_plane(&r, obj, &intr);
			break;
		case SPHERE:
			int_count += intersect_sphere(&r, obj, &intr);
			break;
		case CYLINDER:
			break;
		default:
			printf("Unknown object type %d\n", obj.type);
		}
	}
	return (!int_count);
}

/* fonctionne pas bien */
inline color fresnel(vec3 view, intersection intr)
{
	color frnl = vector_init(0,0,0);
	vec3 fac1 = vector_minus(vector_init(1,1,1), intr.mat.ks);
	float fac2 = 1 - powf(vector_dot(intr.normal, view), 5.0f);
	vec3 prod = vector_float_mul(fac2,fac1);
	frnl = vector_add(intr.mat.ks, prod);
	return frnl;
}

color shade(light lht, vec3 view, intersection intr)
{
	vec3 l = vector_normalized(vector_minus(lht.position, intr.position));
	vec3 h = vector_normalized(vector_add(l, view));

	float nl = vector_dot(l, intr.normal);
	float diffuse_intensity = clamp(nl, 0.0f, 1.0f);

	float nh = vector_dot(h, intr.normal);
	float specular_intensity = powf(clamp(nh, 0.0f, 1.0f), intr.mat.shininess);

	float const1 = (intr.mat.shininess + 8)/(8*M_PI);
	vec3 m_kd = vector_float_mul(1/M_PI, intr.mat.kd);

	vec3 col = vector_float_mul(diffuse_intensity, lht.col);
	vec3 kcol = vector_add(m_kd, vector_float_mul(specular_intensity*const1,intr.mat.ks));
	//vec3 kcol = vector_add(m_kd, vector_float_mul(specular_intensity*const1,fresnel(view,intr)));

	return vector_vec_mul(vector_add(m_kd, kcol), col);
}

color get_light(light *li, int n_li, object *scene, int n_obj, ray r, intersection intr)
{
	color c = vector_init(0,0,0);
	for(int k=0; k<n_li; k++) {
		/* support des ombres */
		if(is_viewable(scene, n_obj, intr.position, li[k].position))
			/* calcul de l'éclairage */
			c = vector_add(c, shade(li[k], vector_float_mul(-1.0,r.dir), intr));
	}
	return c;
}

color trace_ray(light *li, int n_li, object *scene, int n_obj, ray *r)
{
	color c = vector_init(0,0,0);
	intersection intr;
	int c_intr = 0;

	for(int k=0; k<n_obj; k++) {
		object obj = scene[k];
		switch(obj.type) {
		case PLANE:
			if(intersect_plane(r, obj, &intr)) c_intr++;
			break;
		case SPHERE:
			if(intersect_sphere(r, obj, &intr)) c_intr++;
			break;
		case CYLINDER:
			break;
		default:
			printf("Unknown object type %d\n", obj.type);
		}

	}
	if(c_intr) {
		c = vector_add(c, get_light(li, n_li, scene, n_obj, *r, intr));

		if(r->depth >= 10) return vector_init(0,0,0);
		else {
			r->depth++;
			r->dir = vector_reflect(r->dir, intr.normal);
			r->orig = vector_add(vector_float_mul(0.0001f, r->dir), intr.position);
			color ref_color = vector_clamp(vector_vec_mul(intr.mat.ks, trace_ray(li, n_li, scene, n_obj, r)), 0.0f, 1.0f);
			//color ref_color = vector_clamp(vector_float_mul(intr.mat.reflect_coef, trace_ray(li, n_li, scene, n_obj, r)), 0.0f, 1.0f);
			c = vector_add(c, ref_color);			
		}
	} else c = vector_init(0.2,0.2,0.6);


	return c;
}

void* raytrace(void *context)
{
	render_job *infos = (render_job*)context;

	time_t begin,end;
	stats_job *stats = infos->stats;
	stats->tot_rows = infos->end_x - infos->start_x;

	double delta_time=0;
	double sum_time=0;
	double mid_time=0;
	double eta_time=0;
	int curr_rows=0;
	int tot_rows = stats->tot_rows;
	int start_x = infos->start_x;

	camera cam = infos->cam;

	for(int i=infos->start_x; i<infos->end_x; i++) {
		time(&begin);
		for(int j=0; j<HEIGHT; j++) {
			vec3 xr = cam.xdir;
			vec3 yr = vector_float_mul((float)(1/cam.aspect), cam.ydir);
			vec3 zr = cam.center;

			float const1 = ((i+0.5f)-(WIDTH/2.0f))/(WIDTH/2.0f);
			float const2 = ((j+0.5f)-(HEIGHT/2.0f))/(HEIGHT/2.0f);
			vec3 d = vector_add(vector_add(zr, vector_float_mul(const1, xr)), vector_float_mul(const2, yr));
			d = vector_normalized(d);

			ray r;
			ray_init(&r, cam.position, d, 0, 1000.0f, 0.0f);

			infos->img[i+j*WIDTH] = trace_ray(infos->li, infos->n_li, infos->scene, infos->n_obj, &r);
		}
		time(&end);
		//delta_time = (double)clk / (CLOCKS_PER_SEC);
		delta_time = difftime(end, begin);
		sum_time += delta_time;
		curr_rows = (i - start_x + 1);
		mid_time = sum_time / (i - start_x);
		eta_time = mid_time * (tot_rows - curr_rows);

		stats->delta_time = delta_time;
		stats->sum_time = sum_time;
		stats->mid_time = mid_time;
		stats->curr_rows = curr_rows;
		stats->eta_time = eta_time;

		//printf("%d / %d, AVG.: %f s., ETA.: %f s., TOT.: %f s.\n",i+1, infos->end_x, mid_time, mid_time*(WIDTH-i), sum_time);
	}

}

/*
void raytrace(render_job *infos);
{
	clock_t begin, end;
	double delta_time;
	double sum_time;
	double mid_time;

	initscr();
	noecho();
	curs_set(FALSE);

	for(int i=s_i; i<WIDTH; i++)
	{
		clear();
		begin = clock();
		for(int j=s_j; j<HEIGHT; j++)
		{
			vec3 xr = cam.xdir;
			vec3 yr = vector_float_mul((float)(1/cam.aspect), cam.ydir);
			vec3 zr = cam.center;

			float const1 = ((i+0.5f)-(WIDTH/2.0f))/(WIDTH/2.0f);
			float const2 = ((j+0.5f)-(HEIGHT/2.0f))/(HEIGHT/2.0f);
			vec3 d = vector_add(vector_add(zr, vector_float_mul(const1, xr)), vector_float_mul(const2, yr));
			d = vector_normalized(d);

			ray r;
			ray_init(&r, cam.position, d, 0, 1000.0f, 0.0f);

			//float sc = vector_dot(vector_normalized(d), vector_normalized(cam.zdir));
			//sc = sc*sc*sc*sc;
			//img[i+j*WIDTH] = vector_init(sc,sc,sc);
			//printf("%f\n",sc);
			//printf("(%d,%d) #%f -> (%f,%f,%f)\n",i,j,vector_norm(r.dir),r.dir.x,r.dir.y,r.dir.z);
			img[i+j*WIDTH] = trace_ray(li, n_li, scene, n_obj, &r);
		}
		end = clock();
		delta_time = (double)(end - begin) / CLOCKS_PER_SEC;
		sum_time += delta_time;
		mid_time = sum_time / i;

		mvprintw(1, n_thr, "%d / %d, AVG.: %f s., ETA.: %f s., TOT.: %f s.\n",i+1, WIDTH, mid_time, mid_time*(WIDTH-i), sum_time);
		refresh();
	}
	curs_set(TRUE);
}
*/