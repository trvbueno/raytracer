#ifndef __UTILS_H__
#define __UTILS_H__

inline float clamp(float v, float a, float b)
{
	return v<a?a:v>b?b:v;
}

inline float (max)(float a, float b)
{
	return a>b?a:b;
}

inline float (min)(float a, float b)
{
	return a<b?a:b;
}

inline float (abs)(float v)
{
	return v>0?v:-v;
}

#endif
