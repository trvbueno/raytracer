#ifndef __RAYTRACER_H__
#define __RAYTRACER_H__

#include "ray.h"
#include "scene.h"
#include "globals.h"
#include <stdbool.h>

typedef struct intersection_s {
	vec3 normal;
	point3 position;
	material mat;
} intersection;

typedef struct stats_job_s {
	double delta_time;
	double sum_time;
	double mid_time;
	double eta_time;
	int curr_rows;
	int tot_rows;
} stats_job;

typedef struct render_job_s {
	light *li;
	int n_li;
	object *scene;
	int n_obj;
	camera cam;
	int n_thr;
	int start_x;
	int end_x;
	color *img;
	stats_job *stats;
} render_job;

void* raytrace(void *context);

#endif
