CC=g++
OPT=-march=native -Ofast -m64 -flto -DUSE_INLINE_MATH
EXEC=mrt

all: $(EXEC)

vector.o: vector.c
	$(CC) -o vector.o -c vector.c $(OPT)

scene.o: scene.c
	$(CC) -o scene.o -c scene.c $(OPT)

raytracer.o: raytracer.c
	$(CC) -o raytracer.o -c raytracer.c $(OPT)

main.o: main.c
	$(CC) -o main.o -c main.c $(OPT)

$(EXEC): vector.o scene.o raytracer.o main.o
	$(CC) vector.o scene.o raytracer.o main.o -lpthread -lcurses -lfreeimage -I FreeImage/Dist/ -L FreeImage/Dist/ -lm -o $(EXEC)
	
clean:
	rm -rf *.o $(EXEC)
